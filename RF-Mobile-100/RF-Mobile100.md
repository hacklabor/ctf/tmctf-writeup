# RF/MOBILE 100

given file: received_rf.pickle

[received_rf.pickle](received_rf.pickle)



----


- analyses the date with python matplotlib.pyplot

- you will see the Bit length is exactly 100 samples and "low Bit" has no noise

  ![Figure_1](./Pic/Figure_1.png)[Figure_1](./Pic/Figure_1.png)

  ![Figure_2](./Pic/Figure_2.png)[Figure_2](./Pic/Figure_2.png)

  

-  the rest do the python script to get the bits and then decoded to ASCII you will get the Flag

  

![Flag.png](./Pic/Flag.png)[flag.png](./Pic/Flag.png)

```python
import pickle
import numpy
import matplotlib.pyplot as plt


def load_var(var_name):
    fid = open(var_name + '.pickle',"rb")

    data = pickle.load(fid)
    fid.close()
    return data


def main():
    data = load_var('received_rf')
    i=0
    bin = ""
    for p in data:
        print(str(i)+" : "+str(p))
        i+=1

        if i==50:

            if p==0.0  or p==-0.0:
                bin+=("0")
                print(str(i) + " : " + str(p)+"(0)")
            else:
                bin+="1"
                print(str(i) + " : " + str(p) + "(1)")
        if i==100:
            i=0
    print (bin)
    z=0
    b = ""
    flag =""
    for z1 in bin:
        b+=str(z1)
        z+=1
        if z==8:
            flag+= chr(int(b,2))
            b=""
            z=0
    print(flag)
    plt.plot(data)
    plt.show()


if __name__ == '__main__':
    main()
```

----

# TMCTF{5GhereWeGooo}

----

## Team: Hacklabor; Time to solve appr. 1 h

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />